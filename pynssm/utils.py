def normalize_newlines(s):
    return s.replace('\r\n', '\n').replace('\r', '\n')
