import subprocess

from .constants import EXE
from .utils import normalize_newlines

class NSSM(object):

    def __init__(self, service_name):
        self.service_name = service_name

    def docommand(self, command, *args):
        procargs = self.get_args(command, *args)
        completed = subprocess.run(procargs, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        # nssm says it tries to detect unicode and ANSI and return utf16 if unicode.
        # https://nssm.cc/usage (search for "utf-16")
        completed.stdout = normalize_newlines(completed.stdout.decode('utf-16'))
        completed.stderr = normalize_newlines(completed.stderr.decode('utf-16'))
        return completed

    def get_args(self, command, *args):
        return [EXE, command, self.service_name] + list(args)

    def install(self, program, *program_arguments):
        return self.docommand(INSTALL, service_name, program, *program_arguments)

    def remove(self, noconfirm=True):
        if noconfirm:
            # `nssm remove <service> confirm` is for not-confirming
            args = ['confirm']
        else:
            args = []
        return self.docommand(REMOVE, service_name, *args)

    def restart(self):
        return self.docommand(RESTART, service_name)

    def set_(self, parameter, value):
        return self.docommand(SET, service_name, parameter, value)

    def start(self):
        return self.docommand(START, service_name)

    def status(self):
        return self.docommand(STATUS, service_name)

    def stop(self):
        return self.docommand(STOP, service_name)
