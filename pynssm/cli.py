import argparse

from .constants import COMMANDS

def add_dry(parser):
    parser.add_argument('--dry', action='store_true', help='Dry run.')

def add_nssm_parsers(subparser):
    """
    Add nssm commands to a `argparse.subparser`.
    """
    parsers = {}
    for command in COMMANDS:
        parser = subparser.add_parser(command)
        parsers[command] = parser
        parser.add_argument('service_name', help='Name of Windows service.')
        parser.set_defaults(command=command)
        add_dry(parser)

    parsers['install'].add_argument('program', help='Path to program.')
    parsers['install'].add_argument('program_arguments', nargs='*', help='Arguments for program.')

    parsers['set'].add_argument('parameter', help='Parameter name.')
    parsers['set'].add_argument('value')
