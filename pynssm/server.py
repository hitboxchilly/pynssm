from http import HTTPStatus
from http.server import BaseHTTPRequestHandler, HTTPServer

class TestHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        self.send_response(HTTPStatus.OK)
        self.send_header('Content-type', 'text/html')
        self.end_headers()
        self.wfile.write(b'pynssm service is working.')
        return


def runwebserver():
    host, port = server_address = ('localhost', 5000)
    httpd = HTTPServer(server_address, TestHandler)
    print('Visit %s:%s' % (host, port))
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        httpd.socket.close()
