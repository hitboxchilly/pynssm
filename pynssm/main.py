import argparse
import os
import subprocess
import sys

from . import cli, core, server

def install_test():
    install('pynssm-test-service', sys.executable, os.path.abspath(__file__), 'runwebserver')

def main():
    """
    Python command-line nssm.
    """
    parser = argparse.ArgumentParser(description=main.__doc__, prog='pynssm')

    subparsers = parser.add_subparsers()
    cli.add_nssm_parsers(subparsers)

    p = subparsers.add_parser('runwebserver')
    p.set_defaults(func=server.runwebserver)

    p = subparsers.add_parser('install-test')
    cli.add_dry(p)
    p.set_defaults(func=install_test)

    args = parser.parse_args()

    if args.command not in ('runwebserver', 'install-test'):
        nssm = core.NSSM(args.service_name)
        if args.dry:
            print(subprocess.list2cmdline(nssm.get_args(args.command)))

    #XXX: development
    return

    func = args.func
    del args.func
    if hasattr(args, 'service_name'):
        completed = func(args.service_name)
    else:
        completed = func()

    if completed is None:
        return

    print(completed)
