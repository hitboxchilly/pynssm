from distutils.core import setup

setup(name='pynssm',
      version='0.1',
      description='Python interface to nssm',
      author='Carl Harris',
      author_email='elgoogemail2007@gmail.com',
      packages=['pynssm'])
